package br.com.qualirede.crawler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qualirede.crawler.domain.Item;
import br.com.qualirede.crawler.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository repository;
	
	
	public Item save(Item item){
		return repository.save(item);
	}

}
