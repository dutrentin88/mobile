package br.com.qualirede.crawler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qualirede.crawler.domain.Lote;
import br.com.qualirede.crawler.repository.LoteRepository;

@Service
public class LoteService {
	
	@Autowired
	private LoteRepository repository;
	
	public Lote findByNumeroLote(String numeroLote){
		return repository.findByNumeroLote(numeroLote);
	}
	
	public Lote save(Lote lote){
		return repository.save(lote);
	}

}
