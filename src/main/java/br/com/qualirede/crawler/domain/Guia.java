package br.com.qualirede.crawler.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="guia")
@NamedQuery(name="Guia.findAll", query="SELECT g FROM Guia g ")
public class Guia implements Serializable{

	private static final long serialVersionUID = 7290313796710934042L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id; 
	
	private String nrGuia;
	
	private Long cdGuia;
//	
//	@OneToMany(mappedBy = "guias", targetEntity = Lote.class, fetch = FetchType.LAZY)
//	private Lote lote;
	
//	@ManyToOne(cascade = {CascadeType.PERSIST }, fetch=FetchType.EAGER)
//	@JoinColumn(name = "id", referencedColumnName = "id")
//	private Item itens;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrGuia() {
		return nrGuia;
	}

	public void setNrGuia(String nrGuia) {
		this.nrGuia = nrGuia;
	}

	public Long getCdGuia() {
		return cdGuia;
	}

	public void setCdGuia(Long cdGuia) {
		this.cdGuia = cdGuia;
	}
//
//	public Item getItens() {
//		return itens;
//	}
//
//	public void setItens(Item itens) {
//		this.itens = itens;
//	}
	
}
