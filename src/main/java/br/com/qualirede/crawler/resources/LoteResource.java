package br.com.qualirede.crawler.resources;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.qualirede.crawler.domain.Guia;
import br.com.qualirede.crawler.domain.Item;
import br.com.qualirede.crawler.domain.Lote;
import br.com.qualirede.crawler.service.GuiaService;
import br.com.qualirede.crawler.service.ItemService;
import br.com.qualirede.crawler.service.LoteService;

@RestController
@RequestMapping(value = "/lote")
public class LoteResource {
	
	@Autowired
	private LoteService loteService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private GuiaService guiaService;
	
	@RequestMapping(value = "/{numeroLote}", method = RequestMethod.GET)
	public Lote buscaPorNumeroLote(@PathVariable("numeroLote") String numeroLote) {
		System.out.println("Entrou buscar lote");
		return loteService.findByNumeroLote(numeroLote);
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
	public String cadastrarLote() {
		System.out.println("Entrou cadastrar lote");
		try {

			Lote novoLote = new Lote();
			novoLote.setCdLote(100L);
			novoLote.setNrLote("434930");
			loteService.save(novoLote);
			
			Guia novaGuia = new Guia();
			novaGuia.setCdGuia(30L);
			novaGuia.setNrGuia("37329832");
			guiaService.save(novaGuia);
			
			Item novoItem = new Item();	
			novoItem.setValorTotal(new BigDecimal(105.00));
			itemService.save(novoItem);
			
			
		}catch (Exception e) {
			return "Erro ao cadastrar lote";
		}
		
		return "Lote cadastrado com sucesso";
	}

}
