package br.com.qualirede.crawler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qualirede.crawler.domain.Guia;
import br.com.qualirede.crawler.repository.GuiaRepository;

@Service
public class GuiaService {
	
	@Autowired
	private GuiaRepository repository;
	
	public Guia save(Guia guia){
		return repository.save(guia);
	}

}
