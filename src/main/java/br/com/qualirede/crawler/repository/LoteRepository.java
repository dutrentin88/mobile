package br.com.qualirede.crawler.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.qualirede.crawler.domain.Lote;

public interface LoteRepository extends JpaRepository<Lote, Long> {

	@Query(value = "SELECT l FROM Lote  l where l.nrLote = :numeroLote  ")
	public Lote findByNumeroLote(@Param("numeroLote") String numeroLote);
	
}
