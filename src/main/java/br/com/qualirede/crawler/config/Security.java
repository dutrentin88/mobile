//package br.com.qualirede.crawler.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@EnableWebSecurity
//public class Security extends WebSecurityConfigurerAdapter {
//	
//	@Autowired
//	public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
//		authenticationManagerBuilder.inMemoryAuthentication()
//									.withUser("eduardo")
//									.password("eduardo")
//									.roles("USER");
//	}
//	
//	protected void configure(HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.
//				authorizeRequests()
//				.antMatchers("/h2-console/**").permitAll()
//				.anyRequest().authenticated()
//				.and()
//					.httpBasic()
//				.and()
//					.csrf().disable();
//	}
//
//}
