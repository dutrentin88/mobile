package br.com.qualirede.crawler.exceptions;

public class FonteNotFoundedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FonteNotFoundedException(String mesagem) {
		super(mesagem);
	}
	
	public FonteNotFoundedException(String mesagem, Throwable causa) {
		super(mesagem, causa);
	}
	
}
