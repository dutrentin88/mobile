//package br.com.qualirede.crawler.resources;
//
//import java.net.URI;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import br.com.qualirede.crawler.domain.old.Conselho;
//import br.com.qualirede.crawler.domain.old.Especialidade;
//import br.com.qualirede.crawler.domain.old.EspecialidadePrestador;
//import br.com.qualirede.crawler.domain.old.EspecialidadePrestadorId;
//import br.com.qualirede.crawler.domain.old.Execucao;
//import br.com.qualirede.crawler.domain.old.Fonte;
//import br.com.qualirede.crawler.domain.old.Prestador;
//import br.com.qualirede.crawler.exceptions.FonteNotFoundedException;
//import br.com.qualirede.crawler.service.old.ConselhoService;
//import br.com.qualirede.crawler.service.old.EspecialidadeService;
//import br.com.qualirede.crawler.service.old.ExecucaoService;
//import br.com.qualirede.crawler.service.old.FonteService;
//import br.com.qualirede.crawler.service.old.PrestadorService;
//
///**
// * Resource REST de Fonte com prestadores
// * @author Eduardo A. Trentin Borges
// */
//
//@RestController
//@RequestMapping(value = "/fontes")
//public class FonteResources {
//	
//	@Autowired
//	private FonteService fonteService;
//	
//	@Autowired
//	private PrestadorService prestadorService;
//	
//	@Autowired
//	private EspecialidadeService especialidadeService;
//	
//	@Autowired
//	private ConselhoService conselhoService;
//	
//	@Autowired
//	private ExecucaoService execucaoService;
//	
//	private Fonte fonteFiltrada;
//	
//	/** Método de busca por cidade com paginação.
//	 *  Deve-se informar /parte do nome da cidade/pagina inicial / pagina final
//	 * @param String cidade
//	 * @param String pgInit
//	 * @param String pgEnd
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/{cidade}/{pgInit}/{pgEnd}", method = RequestMethod.GET)
//	public ResponseEntity<List<Fonte>> buscaPorCidade(@PathVariable("cidade") String cidade,
//			@PathVariable("pgInit") String pgInitString, @PathVariable("pgEnd") String pgEndString) {
//		if(cidade.length() < 5){
//			return (ResponseEntity<List<Fonte>>) ResponseEntity.badRequest();
//		}
//		int pgInit = 0;
//		int pgEnd = 0;
//		
//		try{
//			pgEnd = Integer.parseInt(pgEndString);
//			if((pgEnd - pgInit) > 100){
//				return ResponseEntity.status(HttpStatus.INSUFFICIENT_STORAGE).body(null);
//			}
//		}catch(Exception ex){
//			return ResponseEntity.status(HttpStatus.INSUFFICIENT_STORAGE).body(null);
//		}
//		
//		Fonte fonte = new Fonte();
//		fonte.setDescricao("CREMESC");
//		fonte.setUrl("www.cremesc.com.br");
//		Pageable pageable = new PageRequest(pgInit, pgEnd);
//		
//		fonte.setPrestadores(prestadorService.findPorCidade(cidade, pageable));
//		fonte.setTotalPrestadores(prestadorService.countFindPorCidade(cidade));
//		
//		List<Fonte> fontes = new ArrayList<Fonte>();
//		fontes.add(fonte);
//		return ResponseEntity.status(HttpStatus.OK).body(fontes);
//	}
//	
////	@RequestMapping(value = "/graphic/{filtro}/{uf}", method = RequestMethod.GET)
////	public DadosGrafico buscaCountPorUf(@PathVariable("filtro") String filtro, @PathVariable("uf") String uf) {
////		DadosGrafico dados = new DadosGrafico();
////		try{
////			if(filtro.equals("SEMFILTRO")){
////				dados = GraphicUtil.preencheEstatisticas(dados,prestadorService);
////			}
////			if(filtro.equals("TODOS")){
////				dados = GraphicUtil.preencheEstatisticasTipoPessoa(dados,uf,prestadorService);				
////			}
////			if(filtro.equals("COMPARACAO")){
////				dados = GraphicUtil.preencheEstatisticasTotal(dados,prestadorService);		
////			}
////			System.out.println();
////		}catch(Exception ex){
////			return null;
////		}
////		
////		return dados;
////	}
//
//
//	/**
//	 *  Método para salvamento de fonte com a lista de prestadores
//	 * 
//	 * @param Fonte fonte
//	 * @return
//	 */
//	@RequestMapping(method = RequestMethod.POST)
//	public ResponseEntity<Void> save(@RequestBody Fonte fonteNova) {
//		System.out.println("Iniciando processo de persistência:" + new Date());
//		if(fonteNova != null){
//			 fonteFiltrada = fonteService.findByDesc(fonteNova.getDescricao());
//			if(fonteFiltrada == null){
//				fonteFiltrada = fonteService.save(new Fonte(fonteNova.getDescricao(), fonteNova.getUrl()));
//			}
//			
//			try{
//				if(fonteFiltrada.getDescricao().equals("CREMESC")){
//					prestadorService.inativaTodosPrestadoresDestaFonte(fonteNova.getDescricao(), fonteNova.getCidadeFiltrada());
//				}
//				
//			}catch(Exception ex){
//				ex.printStackTrace();
//			}
//			System.out.println();
//			
//			
//			fonteNova.getPrestadores().forEach(p -> { 
//				if(p.getNomeFantasia() != null || p.getRazaoSocial() != null){
//					if(p.getConselho() != null & p.getConselho().getNumeroConselho() != null){
//						p.getConselho().setNumeroConselho(p.getConselho().getNumeroConselho().replaceFirst("^0+(?!$)", ""));
//					}
//					List<Prestador> prestadorSelecionadoList = null;
//					Prestador prestadorSelecionado = null;
//					try{
//						String codigoConselho = "";
//						if(codigoConselho != null && !codigoConselho.equals("")){
//							codigoConselho = p.getConselho().getNumeroConselho().trim();
//							codigoConselho = codigoConselho.replaceFirst("^0+(?!$)", "");
//						}
//						if(p.getFonteDesc().equals("CNES") && (p.getNumeroCnes() != null && !p.getNumeroCnes().equals(""))){
//							prestadorSelecionadoList = prestadorService.findPorCNES(p.getNumeroCnes());
//						}else{
//							prestadorSelecionadoList = prestadorService.find(codigoConselho , p.getConselho().getNomeConselho().trim());
//						}
//					}catch(Exception ex){
//						ex.printStackTrace();
//						System.out.println("Código conselho duplicado " + p.getConselho().getNumeroConselho() );
//					}
//					if(prestadorSelecionadoList != null && prestadorSelecionadoList.size() > 0){
//						prestadorSelecionado = prestadorSelecionadoList.get(0);
//						prestadorSelecionadoList = null;
//					}
//					
//					if(prestadorSelecionado == null){
//						List<Especialidade> especialidadesPrestador = new ArrayList<Especialidade>();
//						
//						verificaSeConselhoExiste(p, fonteNova.getDescricao());
//						
//						setaCamposObrigatorios(p);
//						
//						try{
//							p.setFonte(fonteFiltrada);
//							p.setFlagAtivo(true);
//							
//							setaCidade(fonteNova, p);
//							p.setFonteDesc(fonteFiltrada.getDescricao());
//							especialidadesPrestador.addAll(p.getEspecialidades());
//							p = prestadorService.save(p);
//							//por ser transient ao salvar a lista é limpa. Assim salvamos a lista e retornamos para que possa ser percorrida
//							p.setEspecialidades(especialidadesPrestador);
//							salvaEspecialidades(p);
//							especialidadesPrestador = null;
//						}catch(Exception ex){
//							ex.printStackTrace();
//						}
//					}else{
//						try{
//							p.setId(prestadorSelecionado.getId());
//							p.getEndereco().setId(prestadorSelecionado.getEndereco().getId());
//							verificaSeConselhoExiste(p, fonteNova.getDescricao());
//							p.setFonteDesc(fonteFiltrada.getDescricao());
//							p.setFonte(fonteFiltrada);
//							
//							setaCidade(fonteNova, p);
//							
//							setaCamposObrigatorios(p);
//							salvaEspecialidades(p);
//							p = prestadorService.update(p);
//						}catch(Exception ex){
//							ex.printStackTrace();
//						}
//					}
//				}
//			  }
//			);
//			
//		}
//		
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
//				.path("/{id}").buildAndExpand(fonteNova.getId()).toUri();
//		return ResponseEntity.created(uri).build();
//	}
//
//	@RequestMapping(value = "/execucao/{fonteExecutada}/{dataExecucao}", method = RequestMethod.GET)
//	public Execucao registraExecucao(@PathVariable("fonteExecutada") String fonteExecutada,
//				@PathVariable("dataExecucao") String dataExecucao) throws ParseException {
//		dataExecucao = dataExecucao.replaceAll("__", ":");
//		dataExecucao = dataExecucao.replaceAll("_", "-");
//		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date data = formato.parse(dataExecucao);
//		
//		System.out.println("Salva execução:" + new Date());
//		Execucao execucao = null;
//		List<Execucao> execucaoList = execucaoService.findExecucaoAberta(fonteExecutada);
//		if(execucaoList != null && execucaoList.size()> 0){
//			//Possui uma execução aberta para esta fonte
//			Execucao execucaoFinalizar = execucaoList.get(0);
//			execucaoFinalizar.setDataFinalizacao(new Date());
//			
//			try{
//				execucaoService.save(execucaoFinalizar);
//				return execucaoFinalizar;
//			}catch(Exception ex){
//				ex.printStackTrace();
//			}
//		}else{
//			execucao = new Execucao();
//			execucao.setDataExecucao(data);
//			execucao.setFonteExecutada(fonteExecutada);
//			try{
//				execucaoService.save(execucao);
//				return execucao;
//			}catch(Exception ex){
//				ex.printStackTrace();
//			}
//		}
//		
//		return execucao;
//	}
//	
//	private void setaCidade(Fonte fonteNova, Prestador p) {
//		if(p.getEndereco() != null && p.getEndereco().getCidade() != null && !p.getEndereco().getCidade().equals("")){
//			p.setCidadeDesc(p.getEndereco().getCidade());
//		}else{
//			p.setCidadeDesc(fonteNova.getCidadeFiltrada());
//		}
//	}
//
//	private void salvaEspecialidades(Prestador p) {
//		if(p.getEspecialidades() != null && p.getEspecialidades().size() > 0){
//			
////			especialidadePrestadorService.deletaPorPrestador(p);
//			
//			p.setEspecialidadesPrest(new ArrayList<EspecialidadePrestador>());
//			for(Especialidade espec : p.getEspecialidades()){
//				Especialidade especialidadeFiltrada = especialidadeService.findByDescricao(espec.getDescricao());
//				if(especialidadeFiltrada == null){
//					especialidadeFiltrada = especialidadeService.save(espec);
//				}
//				
//				EspecialidadePrestador espPrest = new EspecialidadePrestador();
//				espPrest.setId(new EspecialidadePrestadorId(especialidadeFiltrada.getId(), p.getId()));
//				espPrest.setPrestador(p);
//				espPrest.setEspecialidade(especialidadeFiltrada);
//				espPrest.setRqe(espec.getRegistro());
//				if(!p.getEspecialidadesPrest().contains(espPrest)){
//					p.getEspecialidadesPrest().add(espPrest);
//				}
////				espPrest = null;
//			}
//			try{
//				p = prestadorService.update(p);
//			}catch(Exception ex){
//				ex.printStackTrace();
//				System.out.println("Especialidade duplicada para este prestador.");
//			}
//			
//		}
//	}
//
//	private void setaCamposObrigatorios(Prestador p) {
////		p.setFonte(fonteFiltrada);
//		p.setFlagAtivo(true);
//		p.setDataCadastramento(new Date());
//	}
//
//	private void verificaSeConselhoExiste(Prestador p, String conselho) {
//		if(p.getConselho() == null || p.getConselho().getNumeroConselho() == null){
//			String conselhoDefault = "Sem conselho";
//			if(conselhoService.findByDescricao(conselhoDefault) == null){
//				conselhoService.carregaConselhoDefault();
//			}
//			p.setConselho(conselhoService.findByDescricao(conselhoDefault));
//		}else{
//			Conselho conselhoFiltrado;
//			if(p.getConselho().getNumeroConselho() == null){
//				conselhoFiltrado = new Conselho();
//				conselhoFiltrado.setNomeConselho(conselho);
//			}else{
//				conselhoFiltrado = conselhoService.findByDescricaoCodigo(p.getConselho().getNomeConselho(), p.getConselho().getNumeroConselho() );
//			}
//			if(conselhoFiltrado == null){
//				conselhoFiltrado = conselhoService.save(p.getConselho());
//			}
//			p.setConselho(conselhoFiltrado);
//		}
//	}
//	
////	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
////	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
////		try {
////			Fonte fonte = fonteService.findById(id);
////			return ResponseEntity.status(HttpStatus.OK).body(fonte);
////		} catch (FonteNotFoundedException e) {
////			return ResponseEntity.notFound().build();
////		}
////		
////	}
//	
//	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
//		try {
//			fonteService.delete(id);
//		} catch (FonteNotFoundedException e) {
//			return ResponseEntity.notFound().build();
//		}
//		
//		return ResponseEntity.noContent().build();
//	}
//	
//	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//	public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody Fonte fonte) {
//		fonte.setId(id);
//		try {
//			fonteService.update(fonte);
//		} catch (FonteNotFoundedException e) {
//			return ResponseEntity.notFound().build();
//		}
//		return ResponseEntity.noContent().build();
//	}
//	
//
//}
