package br.com.qualirede.crawler.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.qualirede.crawler.domain.Guia;

public interface GuiaRepository extends JpaRepository<Guia, Long> {

}
