package br.com.qualirede.crawler.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="lote")
@NamedQuery(name="Lote.findAll", query="SELECT l FROM Lote l ")
public class Lote implements Serializable{
	
	private static final long serialVersionUID = -7626420009457344183L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id; 
	
	@Column(length = 20)
	private String nrLote;
	
	private Long cdLote;
	
//	@ManyToOne(cascade = {CascadeType.PERSIST }, fetch=FetchType.EAGER)
//	@JoinColumn(name = "id", referencedColumnName = "id")
//	private Guia guias;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public Guia getGuias() {
//		return guias;
//	}
//
//	public void setGuias(Guia guias) {
//		this.guias = guias;
//	}

	public Long getCdLote() {
		return cdLote;
	}

	public void setCdLote(Long cdLote) {
		this.cdLote = cdLote;
	}

	public String getNrLote() {
		return nrLote;
	}

	public void setNrLote(String nrLote) {
		this.nrLote = nrLote;
	}
	
}
